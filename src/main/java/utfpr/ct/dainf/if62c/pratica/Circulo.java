/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Niap
 */
public class Circulo extends Elipse{

    public Circulo() {
        super();
    }

    public Circulo(double raio) {
        super(raio, raio);
    }

    public double getRaio() {
        return getR();
    }

    @Override
    public String toString() {
        return getNome() + " [" + getRaio() + "]";
    }
    
}
