/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Niap
 */
public class Elipse extends FiguraComEixos{

    public Elipse() {
        super();
    }

    public Elipse(double r, double s) {
        super(r, s);
    }

    @Override
    public double getArea() {
        return getR() * getS();
    }

    @Override
    public double getPerimetro() {
        return Math.PI*(3*(getR()+getS()) - Math.sqrt((3*getR()+getS())*(getR()+3*getS())));
    }
    
}
