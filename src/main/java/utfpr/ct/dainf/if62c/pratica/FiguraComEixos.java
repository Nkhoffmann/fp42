/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Niap
 */
public abstract class FiguraComEixos extends Poligono {
    private double r;
    private double s;

    public FiguraComEixos() {
    }

    public FiguraComEixos(double r, double s) {
        this.r = r;
        this.s = s;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getS() {
        return s;
    }

    public void setS(double s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return super.toString() + " [" + r + " x " + s + "]";
    }

}